# Boats Group Interview Phase 2

## Why we do this
As part of our multi-step interview phase, we created these exercises to show some of the day-to-day activities a developer would be expected to accomplish as part of our team.

## Things we want to see 
- Use of one or more of our preferred technologies (React, NodeJS, Java)
- Use of industry standard practices (Unit Tests, Integration Tests)
- Use of common sense (Simplicity over Complexity)

## Requirements
John from Marketing would like us to create the Boogle Search Engine that is going to redirect the user to the first result from Google search.
This state-of-the-art search engine should contain:
- an API that accepts a query parameter (/search?q=input) and returns the first result from Google search API for that given search term.
- one landing page that contains one input (type: text) and a search button. When the user clicks on the search button, a call to the above API must be done and the result must be shown on the screen.

Since this is a state-of-the art system, we would like for this to demo-able to Marketing from the internet.

## Milestones
- Implement landing page and API locally
- Publish the landing page to a free cloud-based provider (we recommend Heroku, but it can be whatever you want)
- Publish the API to a free cloud-based provider different from the above provider (we recommend Netlify, but it can be whatever you want)

## What we want
- A Git repository on Bitbucket, Gitlab or Github
- A public URL to use the search page.
